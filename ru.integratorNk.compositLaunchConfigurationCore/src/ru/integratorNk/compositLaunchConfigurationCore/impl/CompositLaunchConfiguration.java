package ru.integratorNk.compositLaunchConfigurationCore.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfigurationType;

import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfiguration;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItem;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItemGroup;

public class CompositLaunchConfiguration implements
		ICompositLaunchConfiguration {


	private TreeSet<CompositLaunchConfigurationItem> items;

	public CompositLaunchConfiguration() {
		this.items = new TreeSet<CompositLaunchConfigurationItem>();
	}

	/**
	 * Construct composit launch configuration with items
	 * 
	 * @param items
	 */
	public CompositLaunchConfiguration(
			TreeSet<CompositLaunchConfigurationItem> items) {
		this.items = items;
	}

	private Set<ICompositLaunchConfigurationItemGroup> getGroupsByItems(
			Set<CompositLaunchConfigurationItem> items) throws CoreException {
		HashMap<ILaunchConfigurationType, CompositLaunchConfigurationItemGroup> typesMap = new HashMap<ILaunchConfigurationType, CompositLaunchConfigurationItemGroup>();

		for (CompositLaunchConfigurationItem item : items) {
			ILaunchConfigurationType groupType = item.getLaunchConfiguration()
					.getType();
			if (typesMap.containsKey(groupType)) {
				typesMap.get(groupType).add(item);
			} else {
				typesMap.put(groupType,
						new CompositLaunchConfigurationItemGroup(groupType,
								item));
			}
		}

		Collection<CompositLaunchConfigurationItemGroup> resultCollection = typesMap
				.values();
		TreeSet<ICompositLaunchConfigurationItemGroup> result = new TreeSet<ICompositLaunchConfigurationItemGroup>();
		for (Iterator<CompositLaunchConfigurationItemGroup> iterator = resultCollection
				.iterator(); iterator.hasNext();) {
			result.add(iterator.next());
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfiguration#getGroupsWithActiveLaunch()
	 */
	@Override
	public Set<ICompositLaunchConfigurationItemGroup> getGroupsWithActiveLaunch()
			throws CoreException {
		TreeSet<CompositLaunchConfigurationItem> itemsForGrouping = new TreeSet<CompositLaunchConfigurationItem>();
		for (CompositLaunchConfigurationItem item : items) {
			if (item.isActive()) {
				itemsForGrouping.add(item);
			}
		}
		return getGroupsByItems(itemsForGrouping);
	}

	/* (non-Javadoc)
	 * @see ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfiguration#getGroups()
	 */
	@Override
	public Set<ICompositLaunchConfigurationItemGroup> getGroups()
			throws CoreException {
		return getGroupsByItems(items);
	}

	/**
	 * Get copy of active items for save 
	 * 
	 * @return
	 */
	public TreeSet<CompositLaunchConfigurationItem> getActiveItemsToStore() {
		TreeSet<CompositLaunchConfigurationItem> resultItems = new TreeSet<CompositLaunchConfigurationItem>();
		for (CompositLaunchConfigurationItem item : items) {
			if (item.isActive()) {
				resultItems.add(new CompositLaunchConfigurationItem(item));
			}
		}
		return resultItems;
	}

	
	/*
	 * Get originals of launch configuration for launch 
	 * (non-Javadoc)
	 * @see ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfiguration#getActiveLaunchConfigurations()
	 */
	@Override
	public Set<ICompositLaunchConfigurationItem> getActiveLaunchConfigurations()
			throws CoreException {

		TreeSet<ICompositLaunchConfigurationItem> resultItems = new TreeSet<ICompositLaunchConfigurationItem>();
		for (CompositLaunchConfigurationItem item : items) {
			if (item.isActive()) {
				resultItems.add(item);
			}
		}
		return resultItems;
	}
}
