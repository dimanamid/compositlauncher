package ru.integratorNk.compositLaunchConfigurationCore.impl;

import java.util.Set;
import java.util.TreeSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfigurationType;

import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItem;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItemGroup;

/**
 * Groups, which based on launch configuration type. Not stores.
 * 
 * @author dnd
 *
 */
public class CompositLaunchConfigurationItemGroup implements
		ICompositLaunchConfigurationItemGroup, Comparable<CompositLaunchConfigurationItemGroup> {
	private ILaunchConfigurationType type;
	private Set<CompositLaunchConfigurationItem> items;

	public CompositLaunchConfigurationItemGroup(ILaunchConfigurationType type,
			CompositLaunchConfigurationItem firstItem) {
		this.type = type;
		this.items = new TreeSet<CompositLaunchConfigurationItem>();
		this.items.add(firstItem);
	}

	@Override
	public String getIdentifier() throws CoreException {
		return type.getIdentifier();
	}

	@Override
	public Set<ICompositLaunchConfigurationItem> getItems() {
		Set<ICompositLaunchConfigurationItem> result = new TreeSet<ICompositLaunchConfigurationItem>();
		for (CompositLaunchConfigurationItem item : items) {
			result.add(item);
		}
		return result;
	}

	public void add(CompositLaunchConfigurationItem item) {
		items.add(item);
	}

	@Override
	public String getName() {
		return type.getName();
	}


	@Override
	public int compareTo(CompositLaunchConfigurationItemGroup o) {
		try {
			return (o.getIdentifier()
					.compareTo(getIdentifier()));
		} catch (CoreException e) {
			return 1;
		}
	}

}
