package ru.integratorNk.compositLaunchConfigurationCore.impl;

import java.io.Serializable;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;

import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItem;
import ru.integratorNk.compositLaunchConfigurationCore.meta.Mode;

/**
 * Class stores launchConfiguration, mode of launch, activity. 
 * To save use copy constructor.
 * @author dnd
 *
 */
public class CompositLaunchConfigurationItem implements
		ICompositLaunchConfigurationItem, Serializable, Comparable<CompositLaunchConfigurationItem> {

	private static final long serialVersionUID = 4765734199519936889L;

	private ILaunchConfiguration launchConfiguration = null;

	private Mode mode = Mode.INHERIT;
	private boolean active = false;
	private String identifier = "";

	
	/**
	 * Clone constructor, which ignore "launchConfiguration" field
	 * @param clone
	 */
	public CompositLaunchConfigurationItem(CompositLaunchConfigurationItem clone) {
		this.active = clone.active;
		this.identifier = clone.identifier;
		this.mode = clone.mode;
	}

	
	public CompositLaunchConfigurationItem(
			ILaunchConfiguration launchConfiguration) throws CoreException {
		this.launchConfiguration = launchConfiguration;
		setIdentifier(launchConfiguration.getName());
	}

	public void InitFromStored(ILaunchConfiguration launchConfiguration) {
		if (launchConfiguration == null) {
			throw new IllegalArgumentException("Launch configuration is not defined"); 
		}

		this.launchConfiguration = launchConfiguration;
		active = true;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public Mode getMode() {
		return mode;
	}

	@Override
	public void setMode(Mode mode) {
		this.mode = mode;
	}

	@Override
	public ILaunchConfiguration getLaunchConfiguration() {
		return launchConfiguration;
	}

	private void setIdentifier(String value) throws CoreException {
		identifier = value;
	}

	@Override
	public String getIdentifier() throws CoreException {
		return identifier;
	}


	@Override
	public int compareTo(CompositLaunchConfigurationItem arg0) {
		return arg0.identifier.compareTo(identifier);
	}

}
