package ru.integratorNk.compositLaunchConfigurationCore.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.TreeSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;

import ru.integratorNk.compositLaunchConfigurationCore.Activator;
import ru.integratorNk.compositLaunchConfigurationCore.Base64Coder;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfiguration;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationService;

/**
 * Service to manage launch configuration lists
 * 
 * @author dnd
 *
 */
public class CompositLaunchConfigurationService implements
		ICompositLaunchConfigurationService {

	private final ILaunchManager manager;

	public CompositLaunchConfigurationService() {
		this.manager = DebugPlugin.getDefault().getLaunchManager();
	}

	private ILaunchConfiguration[] getLaunchConfigurations()
			throws CoreException {
		return manager.getLaunchConfigurations();
	}

	@Override
	public ICompositLaunchConfiguration getConfigurationFrom(
			ILaunchConfiguration config) throws CoreException, IOException {

		try {
			String value = config.getAttribute(Activator.PLUGIN_ID, "");

			if (value == "")
				return new CompositLaunchConfiguration();

			@SuppressWarnings("unchecked")
			TreeSet<CompositLaunchConfigurationItem> storedItems = (TreeSet<CompositLaunchConfigurationItem>) fromString(value);

			// convert set to map with identifier key
			HashMap<String, CompositLaunchConfigurationItem> storedItemsMap = new HashMap<String, CompositLaunchConfigurationItem>(
					storedItems.size());
			for (CompositLaunchConfigurationItem compositLaunchConfigurationItem : storedItems) {
				storedItemsMap.put(
						compositLaunchConfigurationItem.getIdentifier(),
						compositLaunchConfigurationItem);
			}

			ILaunchConfiguration[] launchConfigurations = getLaunchConfigurations();

			TreeSet<CompositLaunchConfigurationItem> preResult = new TreeSet<CompositLaunchConfigurationItem>();

			// attempt to find stored components for each configuration
			for (int i = 0; i < launchConfigurations.length; i++) {
				ILaunchConfiguration iLaunchConfiguration = launchConfigurations[i];
				boolean isCurrentPlugin = iLaunchConfiguration.getType()
						.getPluginIdentifier()
						.equalsIgnoreCase(Activator.PLUGIN_ID);
											
				if (isCurrentPlugin) {
					continue;
				}

				String currIdentifier = iLaunchConfiguration.getName();
				CompositLaunchConfigurationItem currItem;

				if (storedItemsMap.containsKey(currIdentifier)) {
					currItem = storedItemsMap.get(currIdentifier);
					currItem.InitFromStored(iLaunchConfiguration);
				} else {
					currItem = new CompositLaunchConfigurationItem(
							iLaunchConfiguration);
				}

				preResult.add(currItem);
			}

			return new CompositLaunchConfiguration(preResult);
		} catch (Exception exception) {
			return new CompositLaunchConfiguration();
		}
	}

	@Override
	public void setConfigurationTo(ILaunchConfigurationWorkingCopy config,
			ICompositLaunchConfiguration value) throws IOException {
		if (value == null
				|| ((value instanceof CompositLaunchConfiguration) == false)) {
			throw new IllegalArgumentException("Incorrect launch configuration");
		}

		String stringValue = toString(((CompositLaunchConfiguration) value)
				.getActiveItemsToStore());
		config.setAttribute(Activator.PLUGIN_ID, stringValue);
	}

	/** Read the object from Base64 string. */
	private static Object fromString(String s) throws IOException,
			ClassNotFoundException {
		byte[] data = Base64Coder.decode(s);
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
				data));
		Object o = ois.readObject();
		ois.close();
		return o;
	}

	/** Write the object to a Base64 string. */
	private static String toString(Serializable o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(o);
		oos.close();
		return new String(Base64Coder.encode(baos.toByteArray()));
	}

}
