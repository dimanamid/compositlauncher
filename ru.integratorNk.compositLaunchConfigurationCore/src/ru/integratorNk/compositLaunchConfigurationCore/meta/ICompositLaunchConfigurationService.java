package ru.integratorNk.compositLaunchConfigurationCore.meta;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

public interface ICompositLaunchConfigurationService {
	ICompositLaunchConfiguration getConfigurationFrom(ILaunchConfiguration config) throws CoreException, IOException;
	void setConfigurationTo(ILaunchConfigurationWorkingCopy config, ICompositLaunchConfiguration value) throws IOException;
}
