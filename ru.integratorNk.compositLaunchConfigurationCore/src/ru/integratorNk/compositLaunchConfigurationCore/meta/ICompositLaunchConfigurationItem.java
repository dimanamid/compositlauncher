package ru.integratorNk.compositLaunchConfigurationCore.meta;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;

public interface ICompositLaunchConfigurationItem extends IHasIdentifier {
	boolean isActive();
	void setActive(boolean active);
	Mode getMode();
	void setMode(Mode mode);
	ILaunchConfiguration getLaunchConfiguration();
	String getIdentifier() throws CoreException;
}
