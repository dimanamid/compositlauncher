package ru.integratorNk.compositLaunchConfigurationCore.meta;

import java.util.Set;

public interface ICompositLaunchConfigurationItemGroup extends IHasIdentifier {
	public Set<ICompositLaunchConfigurationItem> getItems();
	public String getName();
}
