package ru.integratorNk.compositLaunchConfigurationCore.meta;

import org.eclipse.core.runtime.CoreException;

public interface IHasIdentifier {
	String getIdentifier() throws CoreException;
}
