package ru.integratorNk.compositLaunchConfigurationCore.meta;

/**
 * Launch configurations run/debug mode
 * 
 * @author dnd
 *
 */
public enum Mode {
	DEBUG("Debug"),
    RUN("Run"),
    INHERIT("Inherit");

    private Mode(final String text) {
        this.text = text;
    }

    private final String text;
    
    @Override
    public String toString() {
        return text;
    }
}
