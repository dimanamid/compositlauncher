package ru.integratorNk.compositLaunchConfigurationCore.meta;

import java.util.Set;

import org.eclipse.core.runtime.CoreException;

public interface ICompositLaunchConfiguration {
	Set<ICompositLaunchConfigurationItemGroup> getGroupsWithActiveLaunch() throws CoreException;
	Set<ICompositLaunchConfigurationItemGroup> getGroups() throws CoreException;
	Set<ICompositLaunchConfigurationItem> getActiveLaunchConfigurations() throws CoreException;
}
