package ru.integratorNk.compositLaunchConfigurationCore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;
import org.eclipse.ui.statushandlers.StatusManager;

import ru.integratorNk.compositLaunchConfigurationCore.impl.CompositLaunchConfigurationService;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfiguration;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItem;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationService;
import ru.integratorNk.compositLaunchConfigurationCore.meta.Mode;

public class CompositLaunchConfigurationDelegate extends
		LaunchConfigurationDelegate {
	private final ICompositLaunchConfigurationService service = new CompositLaunchConfigurationService();

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ILaunchConfigurationDelegate#launch(org.eclipse.debug.core.ILaunchConfiguration, java.lang.String, org.eclipse.debug.core.ILaunch, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void launch(ILaunchConfiguration configuration, String mode,
			ILaunch launch, IProgressMonitor pm) throws CoreException {
		
		SubMonitor monitor = SubMonitor.convert(pm);
		
		ICompositLaunchConfiguration compositLanchConfiguration = null;
		ArrayList<ICompositLaunchConfigurationItem> items = null;
		
		try {
			
			compositLanchConfiguration = service
					.getConfigurationFrom(configuration);
			
			items = new ArrayList<ICompositLaunchConfigurationItem>(
					compositLanchConfiguration.getActiveLaunchConfigurations());
			
		} catch (IOException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
		}

		

		int countOfLaunchConfigurations = items.size();		
		monitor.beginTask("Launch", countOfLaunchConfigurations);

		try {

			List<ILaunch> resultLaunches = new ArrayList<ILaunch>(
					countOfLaunchConfigurations);
			for (ICompositLaunchConfigurationItem item : items) {
				if (monitor.isCanceled()) {
					stopLaunching(resultLaunches);
					monitor.setCanceled(true);
					return;
				}
				try {

					String runMode = mode;
					if (item.getMode() != Mode.INHERIT) {
						runMode = item.getMode().toString().toLowerCase();
					}

					if (this.preLaunchCheck(item.getLaunchConfiguration(),
							runMode, monitor) == false) {
						throw new CoreException(new Status(IStatus.ERROR,
								Activator.PLUGIN_ID, "Launch configuration is not valid."));
					}

					SubProgressMonitor subMonitor = new SubProgressMonitor(
							monitor, 0);
					resultLaunches.add(item.getLaunchConfiguration().launch(
							runMode, subMonitor));
					monitor.subTask(String.format("Launch done: %s",
							item.getIdentifier()));

					monitor.worked(1);

				} catch (CoreException ex) {
					stopLaunching(resultLaunches);
					StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, ex.getMessage(), ex));
					return;
				}
			}
		} finally {
			monitor.done();
		}

	}

	/**
	 * Stop launches from list 
	 * @param stopList
	 */
	private void stopLaunching(List<ILaunch> stopList) {
		try {
			for (ILaunch launch : stopList) {
				for (IProcess process : launch.getProcesses()) {
					if (!process.isTerminated()) {
						process.terminate();
					}
					launch.removeProcess(process);
				}
				for (IDebugTarget debugTarget : launch.getDebugTargets()) {
					launch.removeDebugTarget(debugTarget);
				}
				if (launch.canTerminate())
					launch.terminate();
			}
		} catch (DebugException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
		}
	}
}
