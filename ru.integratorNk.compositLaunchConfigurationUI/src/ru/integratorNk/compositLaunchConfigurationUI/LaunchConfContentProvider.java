package ru.integratorNk.compositLaunchConfigurationUI;

import java.util.ArrayList;
import java.util.Set;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItemGroup;

public class LaunchConfContentProvider implements IStructuredContentProvider {

	@Override
	public void dispose() {

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

	}

	@Override
	public Object[] getElements(Object inputElement) {
		ArrayList<Object> result = new ArrayList<Object>();

		if (inputElement == null)
			return null;

		@SuppressWarnings("unchecked")
		Set<ICompositLaunchConfigurationItemGroup> items = (Set<ICompositLaunchConfigurationItemGroup>) inputElement;
		for (ICompositLaunchConfigurationItemGroup group : items) {
			result.add(group);
			result.addAll(group.getItems());
		}

		return result.toArray();
	}

}
