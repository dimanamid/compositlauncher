package ru.integratorNk.compositLaunchConfigurationUI;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;

import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfiguration;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class CompositeLaunchPage extends Composite {
	private LaunchConfTableViewer table;

	private ICompositLaunchConfiguration configuration;
	private ICompositeLaunchTableOnChange onChangeEventHandler = null;
	
	private boolean isModeShowAll = true;
	private CompositeLaunchItemsTab tab = null;
	public CompositeLaunchPage(Composite parent, CompositeLaunchItemsTab tab) {
		super(parent, SWT.MULTI);
		this.tab = tab;
		setLayout(new FormLayout());

		final Button btnNewButton = new Button(this, SWT.NONE);
		final Button btnNewButton_1 = new Button(this, SWT.NONE);

		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					isModeShowAll = true;
					btnNewButton.setEnabled(false);
					btnNewButton_1.setEnabled(true);
					table.setInput(configuration.getGroups());
				} catch (CoreException e1) {
					Activator.logError(e1);
				}
			}
		});

		FormData fd_btnNewButton = new FormData();
		fd_btnNewButton.top = new FormAttachment(0, 49);
		btnNewButton.setEnabled(false);
		btnNewButton.setLayoutData(fd_btnNewButton);
		btnNewButton.setText("Show all");

		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					isModeShowAll = false;
					table.setInput(configuration.getGroupsWithActiveLaunch());
					btnNewButton.setEnabled(true);
					btnNewButton_1.setEnabled(false);

				} catch (CoreException e1) {
					Activator.logError(e1);
				}
			}
		});
		FormData fd_btnNewButton_1 = new FormData();
		fd_btnNewButton_1.top = new FormAttachment(0, 49);
		fd_btnNewButton_1.left = new FormAttachment(0, 67);
		btnNewButton_1.setLayoutData(fd_btnNewButton_1);
		btnNewButton_1.setText("Show selected");

		Composite composite = new Composite(this, SWT.FILL);
		fd_btnNewButton.left = new FormAttachment(0, 5);
		fd_btnNewButton.left = new FormAttachment(composite, 0, SWT.LEFT);
		composite.setLayout(new FillLayout(SWT.HORIZONTAL));

		FormData fd_composite = new FormData();
		fd_composite.top = new FormAttachment(0, 80);
		fd_composite.bottom = new FormAttachment(100, -5);
		fd_composite.right = new FormAttachment(100, -5);
		fd_composite.left = new FormAttachment(0, 5);
		composite.setLayoutData(fd_composite);
		
		Label lblNewLabel = new Label(this, SWT.NONE);
		FormData fd_lblNewLabel = new FormData();
		fd_lblNewLabel.left = new FormAttachment(0, 5);
		fd_lblNewLabel.top = new FormAttachment(0, 10);
		lblNewLabel.setLayoutData(fd_lblNewLabel);
		lblNewLabel.setText("Select launch configuration. You can set mode (Debug/Run) of their launch.");
		
		try {
			table = new LaunchConfTableViewer(composite);

			table.setOnChangeEventHandler(new ICompositeLaunchTableOnChange() {
				@Override
				public void execute() {
					try {
						if (isModeShowAll) {
							table.setInput(configuration.getGroups());
						} else {
							table.setInput(configuration
									.getGroupsWithActiveLaunch());
						}
						raiseOnChange();
					} catch (CoreException e) {
						Activator.logError(e);
					}
					raiseOnChange();
				}
			});
		} catch (Exception e) {
			Activator.logError(e);

		}
	}


	public ICompositLaunchConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ICompositLaunchConfiguration conf) {
		this.configuration = conf;
		try {
			if (isModeShowAll) {
				table.setInput(configuration.getGroups());
			} else {
				table.setInput(configuration.getGroupsWithActiveLaunch());
			}
		} catch (CoreException e) {
			Activator.logError(e);
		}
	}

	private void raiseOnChange() {
		if (onChangeEventHandler != null) {
			onChangeEventHandler.execute();
		}
		tab.onChanged();
	}
}
