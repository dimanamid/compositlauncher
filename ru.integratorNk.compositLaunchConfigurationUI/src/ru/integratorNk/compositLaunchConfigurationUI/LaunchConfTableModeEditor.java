package ru.integratorNk.compositLaunchConfigurationUI;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItem;
import ru.integratorNk.compositLaunchConfigurationCore.meta.Mode;

public class LaunchConfTableModeEditor extends EditingSupport {

	private ComboBoxViewerCellEditor cellEditor = null;

	public LaunchConfTableModeEditor(ColumnViewer viewer) {
		super(viewer);
		cellEditor = new ComboBoxViewerCellEditor((Composite) getViewer()
				.getControl(), SWT.READ_ONLY);
		cellEditor.setLabelProvider(new LabelProvider());
		cellEditor.setContentProvider(new ArrayContentProvider());
		cellEditor.setInput(Mode.values());
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return cellEditor;
	}

	@Override
	protected boolean canEdit(Object element) {
		if (element instanceof ICompositLaunchConfigurationItem) {
			return true;
		}
		return false;
	}

	@Override
	protected Object getValue(Object element) {
		if (element instanceof ICompositLaunchConfigurationItem) {
			return ((ICompositLaunchConfigurationItem) element).getMode();
		}
		return null;
	}

	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ICompositLaunchConfigurationItem
				&& value instanceof Mode) {
			ICompositLaunchConfigurationItem data = (ICompositLaunchConfigurationItem) element;
			Mode newValue = (Mode) value;
			data.setMode(newValue);
		}
	}
}
