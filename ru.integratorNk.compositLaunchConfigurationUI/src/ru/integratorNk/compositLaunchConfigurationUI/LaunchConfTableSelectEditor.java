package ru.integratorNk.compositLaunchConfigurationUI;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItem;

public class LaunchConfTableSelectEditor extends EditingSupport {
	private final CheckboxCellEditor cellEditor;

	public LaunchConfTableSelectEditor(ColumnViewer viewer) {
		super(viewer);
		cellEditor = new CheckboxCellEditor((Composite) getViewer()
				.getControl(), SWT.NONE);
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return cellEditor;
	}

	@Override
	protected boolean canEdit(Object element) {
		if (element instanceof ICompositLaunchConfigurationItem) {
			return true;
		}

		return false;
	}

	@Override
	protected Object getValue(Object element) {
		if (element instanceof ICompositLaunchConfigurationItem) {
			return ((ICompositLaunchConfigurationItem) element).isActive();
		}
		return null;
	}

	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ICompositLaunchConfigurationItem
				&& value instanceof Boolean) {
			((ICompositLaunchConfigurationItem) element)
					.setActive((Boolean) value);
		}
	}
}
