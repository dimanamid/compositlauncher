package ru.integratorNk.compositLaunchConfigurationUI;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import ru.integratorNk.compositLaunchConfigurationCore.impl.CompositLaunchConfigurationService;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfiguration;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationService;

public class CompositeLaunchItemsTab extends AbstractLaunchConfigurationTab {
	public static final String NAME = "Composite";

	private ICompositLaunchConfigurationService configurationService = new CompositLaunchConfigurationService();
	private CompositeLaunchPage compositeLaunchPage;
	private ICompositLaunchConfiguration compositLaunchConfiguration;
	@Override
	public void createControl(Composite parent) {
		compositeLaunchPage = new CompositeLaunchPage(parent, this);

	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {

	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {

		try {
			compositLaunchConfiguration = configurationService
					.getConfigurationFrom(configuration);
			compositeLaunchPage.setConfiguration(compositLaunchConfiguration);
			
		} catch (CoreException e) {
			Activator.logError(e);
		} catch (IOException e) {
			Activator.logError(e);
		}
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		try {
			configurationService.setConfigurationTo(configuration,
					compositLaunchConfiguration);
		} catch (IOException e) {
			Activator.logError(e);
		}
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Control getControl() {
		return compositeLaunchPage;
	}
	
	public void onChanged()
	{
		updateLaunchConfigurationDialog();
	}
}
