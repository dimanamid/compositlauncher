package ru.integratorNk.compositLaunchConfigurationUI;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItem;
import ru.integratorNk.compositLaunchConfigurationCore.meta.ICompositLaunchConfigurationItemGroup;

public class LaunchConfTableLabelProvider implements ITableLabelProvider,
		ITableColorProvider {

	private static final Image IconActive = AbstractUIPlugin
			.imageDescriptorFromPlugin(Activator.PLUGIN_ID, "icons/1.ico")
			.createImage();
	private static final Image IconNotActive = AbstractUIPlugin
			.imageDescriptorFromPlugin(Activator.PLUGIN_ID, "icons/0.ico")
			.createImage();

	public LaunchConfTableLabelProvider(Display display) {
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
	}

	@Override
	public Color getForeground(Object element, int columnIndex) {
		return null;
	}

	@Override
	public Color getBackground(Object element, int columnIndex) {
		return null;
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		if (element instanceof ICompositLaunchConfigurationItem) {
			ICompositLaunchConfigurationItem item = (ICompositLaunchConfigurationItem) element;

			if (columnIndex == 1) {
				try {
					return DebugUITools.getImage(item.getLaunchConfiguration()
							.getType().getIdentifier());
				} catch (CoreException e) {
					Activator.logError(e);
				}
			}

			if (columnIndex == 0) {
				if (item.isActive()) {
					return IconActive;
				}
				return IconNotActive;
			}
		} else {
			ICompositLaunchConfigurationItemGroup itemGroup = (ICompositLaunchConfigurationItemGroup) element;

			if (columnIndex == 1) {
				try {
					return DebugUITools.getImage(itemGroup.getIdentifier());
				} catch (CoreException e) {
					Activator.logError(e);
				}
			}

		}
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof ICompositLaunchConfigurationItem) {
			ICompositLaunchConfigurationItem item = (ICompositLaunchConfigurationItem) element;
			switch (columnIndex) {
			case 1:
				try {
					return " -> " + item.getIdentifier();
				} catch (CoreException e) {
					Activator.logError(e);
				}
			case 2:
				return item.getMode().toString();
			}
		} else {
			ICompositLaunchConfigurationItemGroup item = (ICompositLaunchConfigurationItemGroup) element;
			switch (columnIndex) {
			case 1:
				return item.getName();
			}

		}

		return null;
	}

}
