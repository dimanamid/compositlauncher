package ru.integratorNk.compositLaunchConfigurationUI;

import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationListener;
import org.eclipse.jface.viewers.ColumnViewerEditorDeactivationEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

public class LaunchConfTableViewer extends TableViewer {

	private ICompositeLaunchTableOnChange onChangeHandler;

	public LaunchConfTableViewer(Composite parent) {
		super(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);

		setContentProvider(new LaunchConfContentProvider());

		TableViewerColumn selectedColumn = new TableViewerColumn(this, SWT.NONE);
		selectedColumn.getColumn().setText("Selected");
		selectedColumn.getColumn().setWidth(60);
		selectedColumn.getColumn().setResizable(true);		
		
		LaunchConfTableSelectEditor activateEditor = new LaunchConfTableSelectEditor(
				selectedColumn.getViewer());
		selectedColumn.setEditingSupport(activateEditor);

		TableViewerColumn columnName = new TableViewerColumn(this, SWT.FILL);
		columnName.getColumn().setText("Name");
		columnName.getColumn().setWidth(300);
		columnName.getColumn().setResizable(true);
		
		
		TableViewerColumn modeColumn = new TableViewerColumn(this, SWT.NONE);
		modeColumn.getColumn().setText("Specific mode");
		modeColumn.getColumn().setWidth(140);
		modeColumn.getColumn().setResizable(false);
		
		
		LaunchConfTableModeEditor modeEditor = new LaunchConfTableModeEditor(
				modeColumn.getViewer());
		modeColumn.setEditingSupport(modeEditor);

		setLabelProvider(new LaunchConfTableLabelProvider(getControl()
				.getDisplay()));
		getTable().setHeaderVisible(true);
		getTable().setLinesVisible(true);

		getColumnViewerEditor().addEditorActivationListener(
				new ColumnViewerEditorActivationListener() {

					@Override
					public void beforeEditorDeactivated(
							ColumnViewerEditorDeactivationEvent event) {
					}

					@Override
					public void beforeEditorActivated(
							ColumnViewerEditorActivationEvent event) {
					}

					@Override
					public void afterEditorDeactivated(
							ColumnViewerEditorDeactivationEvent event) {
						if (onChangeHandler != null)
							onChangeHandler.execute();
					}

					@Override
					public void afterEditorActivated(
							ColumnViewerEditorActivationEvent event) {
					}
				});
	}

	public void setOnChangeEventHandler(ICompositeLaunchTableOnChange handler) {
		this.onChangeHandler = handler;
	}

}
